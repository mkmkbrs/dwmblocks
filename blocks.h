// Modify this file to change status bar output
// Recompile and install using the 'make && sudo make install' command

static const Block blocks[] = {
	/*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/

	{"  ", "~/scripts/bar/bar-cmus-status.sh", 1, 0},

	{"  ", "~/scripts/bar/bar-keylayout.sh 1", 0, 2},

	{"  ", "~/scripts/bar/bar-volume.sh 1", 0, 1},

	{"  ", "~/scripts/bar/bar-ram.sh 1", 3, 0},

	{"  ", "~/scripts/bar/bar-cputemp.sh 1", 5, 0},

	{"  ", "~/scripts/bar/bar-weather.sh 1", 1800, 0},

	{"  ", "~/scripts/bar/bar-freespace.sh 1", 5, 0},

	{"  ", "~/scripts/bar/bar-date.sh", 60, 0},

	{"  ", "~/scripts/bar/bar-time.sh", 60, 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
