# dwmblocks

Modular status bar for dwm written in C.

## Modifying blocks

The status bar is made from text output of commandline programs.

Blocks are added and removed by editing the **blocks.h** header file.

## Scripts

All scripts can be found [here](https://gitlab.com/mkmkbrs/scripts/-/tree/master/bar?ref_type=heads).

## Installation

In the dwmblocks directory run:

```
make && sudo make install
```
